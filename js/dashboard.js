$(document).ready(function () {
    var elements, switcheryOptions;
    elements = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
    switcheryOptions = {
        color: '#1bc98e'
    };
    elements.forEach(function (e) {
        var switchery = new Switchery(e, switcheryOptions);
    });
    changeMultiplier = 0.2;
    window.setInterval(function () {
        var freeSpacePercentage;
        freeSpacePercentage = $('#free-space').text();
        freeSpacePercentage = parseFloat(freeSpacePercentage);
        delta = changeMultiplier * (Math.random() < 0.5 ? -1.0 : 1.0);
        freeSpacePercentage = freeSpacePercentage + freeSpacePercentage * delta;
        freeSpacePercentage = parseInt(freeSpacePercentage);
        $('#free-space').text(freeSpacePercentage + '%');
    }, 2000);
    $('.loading').remove();
    $('[data-toggle="tooltip"]').tooltip();
    $('.round-chart').easyPieChart({
        'scaleColor': false,
        'lineWidth': 20,
        'lineCap': 'butt',
        'barColor': '#6d5cae',
        'trackColor': '#e5e9ec',
        'size': 190
    });
    $('#performance-eval .spider-chart').highcharts({
        chart: {
            polar: true,
            type: 'area'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['Taming', 'Acessory', 'Development', 'Grooming', 'Awareness',
                'Ration'],
            tickmarkPlacement: 'on',
            lineWidth: 0
        },
        yAxis: {
            gridLineInterpolation: 'polygon',
            lineWidth: 0,
            min: 0
        },
        tooltip: {
            shared: true,
            pointFormat: '<span style="color:{series.color}">{series.name}: <b>${point.y:,.0f}</b><br/>'
        },
        legend: {
            align: 'right',
            verticalAlign: 'top',
            y: 70,
            layout: 'vertical'
        },
        series: [{
            name: 'Allocated resources',
            data: [45000, 39000, 58000, 63000, 38000, 93000],
            pointPlacement: 'on',
            color: '#676F84'
        },
            {
                name: 'Spent resources',
                data: [83000, 49000, 60000, 35000, 77000, 90000],
                pointPlacement: 'on',
                color: '#f35958'
            }]
    });
    $('#daily-usage .area-chart').highcharts({
        title: {
            text: '',
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                dataLabels: {
                    enabled: true,
                    style: {
                        fontWeight: '300'
                    }
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Time share',
            data: [
                ['Front yard', 10.38],
                ['Closet', 26.33],
                ['Swim pool', 51.03],
                ['Like a boss', 4.77],
                ['Barking', 3.93]
            ]
        }]
    });
    $('#search-icon').on('click', function (e) {
        e.preventDefault();
        $('form#search').slideDown('fast');
        $('form#search input:first').focus();
    });
    $('form#search input').on('blur', function (e) {
        if ($('#search-icon').is(':visible')) {
            $('form#search').slideUp('fast');
        }
    });
    $('.carousel').carousel({
        interval: 1000
    });
    $('#content').scrollspy({
        target: '#content-spy'
    });
    $('#githubModal').on('show.bs.modal', function (e) {
        var $element = $(this),
            url = 'https://api.github.com/users/{username}';
        title = 'Hi, my name is {name}';
        content = '' +
            '<div class="row">' +
            '<img src="{img}" class="col-sm-3">' +
            '<p class="col-sm-9" id="bio">{bio}</p>' +
            '</div>',
            bio = '' +
                'At moment I have {publicRepo} public repos ' +
                'and {followers} followers.\n' +
                'I joined Github on {dateJoin}';
        url = url.replace(/{username}/, $('#github-username').val());
        $.get(url, function (data) {
            title = title.replace(/{name}/, data.name);
            bio = bio.replace(/{publicRepo}/, data.public_repos).replace(/{followers}/, data.followers).replace(/{dateJoin}/, data.created_at.split('T')[0]);
            content = content.replace(/{img}/, data.avatar_url).replace(/{bio}/, bio);
            $element.find('.modal-title').text(title);
            $element.find('.modal-body').html(content);
            console.log(data);
        });
    });
});
+function ($) {
    var BootstrapCarousel = function (element, options) {
        this.$element = $(element);
        this.options = $.extend({}, BootstrapCarousel.DEFAULTS, options);
        this.addSlide = BootstrapCarousel.prototype.addSlide;
        this.reload = BootstrapCarousel.prototype.load;
        this.init();
    };
    BootstrapCarousel.VERSION = '1.0.0';
    BootstrapCarousel.DEFAULTS = {};
    BootstrapCarousel.prototype = {};

    function Plugin(option) {
        var args = arguments;
        [].shift.apply(args);
        return this.each(function () {
            var $this = $(this),
                data = $this.data('bootstrap-carousel'),
                options = $.extend({}, BootstrapCarousel.DEFAULTS, $this.data(), typeof option == 'object' && option),
                value;
            if (!data) {
                $this.data('bootstrap-carousel', (data = new BootstrapCarousel(this, options)));
            }
            if (typeof option == 'string') {
                if (data[option] instanceof Function) {
                    value = data[option].apply(data, args);
                } else {
                    value = data.options[option];
                }
            }
        })
    }

    var old = $.fn.bCarousel;
    $.fn.bCarousel = Plugin;
    $.fn.bCarousel.Constructor = BootstrapCarousel;
    $.fn.bCarousel.noConflict = function () {
        $.fn.bCarousel = old;
        return this;
    };
    $(window).on('load', function () {
        $('.bootstrap-carousel').each(function () {
            var $carousel = $(this);
            Plugin.call($carousel, $carousel.data());
        })
    });
    BootstrapCarousel.prototype = {
        init: function () {
            if (!this.$element.attr('id')) {
                throw "provide an id";
            }
            this.$slides = this.$element.find('> img');
            this.$element.addClass('slide carousel');
            this.load();
        },
        load: function () {
            this.$element.find('.carousel-inner, .carousel-indicators, .carousel-control').remove();
            this.$slides = this.$element.find('> img');
            this.$slides.hide();
            this.$element.carousel('pause');
            this.$element.removeData('bs.carousel');
            this.$element.append(this.createCarousel());
            this.initPlugin();
        },
        template: {
            slide: '...',
            carouselInner: '...',
            carouselItem: '...',
            carouselIndicator: '...',
            carouselIndicatorItem: '...',
            carouselControls: '...',
        },
        createCarousel: function () {
            var template = '';
            template += this.createSlideDeck();
            if (this.options.indicators) {
                template += this.createIndicators();
            }
            if (this.options.controls) {
                template += this.createControls();
            }
            return template;
        },
        createSlideDeck: function () {
            var slideTemplate = '',
                slide;
            for (var i = 0; i < this.$slides.length; i++) {
                slide = this.$slides.get(i);
                slideTemplate += this.createSlide(
                    i == 0 ? 'active' : '',
                    slide.src,
                    slide.dataset.title,
                    slide.dataset.content
                );
            }
            return this.template.carouselInner.replace(/{innerContent}/, slideTemplate);
        },
        createSlide: function (active, itemImg, itemTitle, itemContent) {
            return this.template.carouselItem.replace(/{activeClass}/, active)
                .replace(/{itemImg}/, itemImg)
                .replace(/{itemTitle}/, itemTitle || this.options.defaultTitle)
                .replace(/{itemContent}/, itemContent || this.options.defaultContent);
        },
        createIndicators: function () {
            var indicatorTemplate = '',
                slide,
                elementId = this.$element.attr('id');
            for (var i = 0; i < this.$slides.length; i++) {
                slide = this.$slides.get(i);
                indicatorTemplate += this.template.carouselIndicatorItem.replace(/{elementId}/, elementId)
                    .replace(/{slideNumber}/, i)
                    .replace(/{activeClass}/, i == 0 ? 'class="active"' : '');
            }
            return this.template.carouselIndicator.replace(/{indicators}/, indicatorTemplate);
        },
        createControls: function () {
            var elementId = this.$element.attr('id');
            return this.template.carouselControls.replace(/{elementId}/g, elementId)
                .replace(/{previousIcon}/, this.options.previousIcon)
                .replace(/{previousText}/, this.options.previousText)
                .replace(/{nextIcon}/, this.options.nextIcon)
                .replace(/{nextText}/, this.options.nextText);
        },
        initPlugin: function () {
            this.$element.carousel({
                interval: this.options.interval,
                pause: this.options.pause,
                wrap: this.options.wrap,
                keyboard: this.options.keyboard
            });
        },
        addSlide: function (itemImg, itemTitle, itemContent) {
            var newSlide = this.template.slide.replace(/{itemImg}/, itemImg)
                .replace(/{itemTitle}/, itemTitle)
                .replace(/{itemContent}/, itemContent);
            this.$element.append(newSlide);
            this.load();
        }
    };
    BootstrapCarousel.DEFAULTS = {
        indicators: true,
        controls: true,
        defaultTitle: '',
        defaultContent: '',
        nextIcon: 'glyphicon glyphicon-chevron-right',
        nextText: 'Next',
        previousIcon: 'glyphicon glyphicon-chevron-left',
        previousText: 'Previous',
        interval: 5000,
        pause: 'hover',
        wrap: true,
        keyboard: true
    };
}(jQuery);